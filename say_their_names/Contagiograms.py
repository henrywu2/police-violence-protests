import contagiograms
from datetime import datetime
import pandas as pd
import json


def parse_ngram(ngram):
    formatted_name = ngram.split()
    formatted_name = ' '.join([formatted_name[0], formatted_name[1]])
    return formatted_name


#names = pd.read_csv('say_their_names/combined_name_rank.csv').sort_values(by='min_rank').loc[:23, 'Unnamed: 0'].squeeze().tolist()
names = ['George Floyd', 'Jacob Blake', 'Breonna Taylor', 'Trayvon Martin', 'Ahmaud Arbery', 'Eric Garner', 'Alton Sterling', 'Sandra Bland', 'Philando Castile', 'Michael Brown', 'Charleena Lyles', 'Tamir Rice']
for i in range(len(names)):
    names[i] = [names[i], 'en']

"""
page1 = []
page2 = []
for i in range(len(names)):
    names[i] = parse_ngram(names[i])
    if i < 12:
        page1.append([parse_ngram(names[i]), 'en'])
    else:
        page2.append([parse_ngram(names[i]), 'en'])

ngrams = {
    "names_1": page1,
    "names_2": page2
}
"""
ngrams = { 'names': names }

contagiograms.plot(
    ngrams,
    'say_their_names/figures/contagiograms/short_term',
    start_date=datetime(2020, 5, 1),
    t1='1W',
    t2=7,
    day_of_the_week=False
)

contagiograms.plot(
    ngrams,
    'say_their_names/figures/contagiograms/long_term',
    t1='1M',
    t2=30,
    day_of_the_week=False
)