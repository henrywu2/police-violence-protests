from datetime import datetime, timedelta, date

from storywrangling import Storywrangler
storywrangler = Storywrangler()

import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import ListedColormap
import matplotlib.dates as dates
import matplotlib.ticker as ticker
#matplotlib.rcParams['font.family'] = 'serif'

import json

from scipy import stats, optimize

import pandas as pd
pd.set_option('display.max_rows', 5000)

import seaborn as sns

import numpy as np

import math

DATES = [[datetime(2014, 11, 24), datetime(2014, 12, 8)],
    [datetime(2015, 7, 13), datetime(2015, 7, 26)],
    [datetime(2016, 7, 5), datetime(2016, 7, 13)],
    [datetime(2017, 8, 12), datetime(2017, 8, 22)], 
    [datetime(2020, 5, 25), datetime(2020, 6, 7)]]

#HELPER METHODS
#Making colormaps
def colormap(name):
  colors = cm.get_cmap('magma_r', 10000)
  if name == 'name_rank':
    scale = np.linspace(1/26, 25/26, 13)
    cmap_scaled = colors(scale)
    return ListedColormap(cmap_scaled)
  elif '2d_hist' in name:
    scale = np.linspace(1/16, 15/16, 8)
    if name == '2d_hist_scaled':
      cscale = []
      cscale.extend(scale[0].repeat(1))
      cscale.extend(scale[1].repeat(4))
      cscale.extend(scale[2].repeat(5))
      cscale.extend(scale[3].repeat(15))
      cscale.extend(scale[4].repeat(25))
      cscale.extend(scale[5].repeat(25))
      cscale.extend(scale[6].repeat(25))
      cscale.extend(scale[7].repeat(381))
      cmap_scaled = colors(cscale)
      return ListedColormap(cmap_scaled)
    elif name == '2d_hist_unscaled':
      cmap = colors(scale)
      return ListedColormap(cmap)
    elif name == '2d_hist_ratios':
      scale = np.linspace(1/14, 13/14, 7)
      cscale = []
      cscale.extend(scale[0].repeat(1))
      cscale.extend(scale[1].repeat(4))
      cscale.extend(scale[2].repeat(5))
      cscale.extend(scale[3].repeat(15))
      cscale.extend(scale[4].repeat(25))
      cscale.extend(scale[5].repeat(25))
      cscale.extend(scale[6].repeat(34))
      cmap_scaled = colors(cscale)
      return ListedColormap(cmap_scaled)
    elif name == '2d_hist_ratios_unscaled':
      scale = np.linspace(1/14, 13/14, 7)
      cmap = colors(scale)
      return ListedColormap(cmap)
  else:
    scale = np.linspace(0.1, 0.9, 5)
    if name in ['heatmap_scaled', 'heatmap_scaled_display']:
      cscale = []
      if name == 'heatmap_scaled':
        #these scales represent 1/100, 1/30, 1/10, 1/3, and 1
        cscale.extend(scale[0].repeat(3000))
        cscale.extend(scale[1].repeat(7000))
        cscale.extend(scale[2].repeat(20000))
        cscale.extend(scale[3].repeat(70000))
        cscale.extend(scale[4].repeat(200000))
        cmap_scaled = colors(cscale)
        cmap_scaled[0, :] = np.array([1, 1, 1, 1])
        cmap_scaled[-1, :] = np.array([0, 0, 0, 1])
        return ListedColormap(cmap_scaled)
      else:
        cscale.extend(scale[0].repeat(23))
        cscale.extend(scale[1].repeat(18))
        cscale.extend(scale[2].repeat(18))
        cscale.extend(scale[3].repeat(18))
        cscale.extend(scale[4].repeat(23))
        cmap_scaled = colors(cscale)
        for i in range(5):
          cmap_scaled[i, :] = np.array([1, 1, 1, 1])
        for i in range(95, 100):
          cmap_scaled[i, :] = np.array([0, 0, 0, 1])
        return ListedColormap(cmap_scaled)
    elif name == 'heatmap_unscaled':
      cmap = colors(scale.repeat(60000))
      cmap[:1, :] = np.array([1, 1, 1, 1])
      cmap[-1:, :] = np.array([0, 0, 0, 1])
      return ListedColormap(cmap)



#Formats a name into a 2-gram.
def parse_name(ngram):
  if ngram == ngram and ngram != 'Name withheld by police':
    formatted_name = ngram.replace('.', '').replace('"', '').replace(',', '').replace('(', '').replace(')', '').split()
    formatted_name = ' '.join([formatted_name[0], formatted_name[-2]]) if formatted_name[-1] in ['Jr', 'Sr', 'II', 'III', 'IV'] else ' '.join([formatted_name[0], formatted_name[-1]])
  else:
    formatted_name = '_UNKNOWN_NAME_'
  return formatted_name



#Get cumulative fatal police shootings
def cumulative(df, metric, date):
  df_count = df[metric].value_counts().to_frame().sort_index()
  df_count.index = pd.to_datetime(df_count.index, infer_datetime_format=True)
  df_count = df_count.loc[df_count.index >= datetime(date.year, date.month, date.day)]
  df_count_zeros = pd.DataFrame(0, index=pd.date_range(start=date, end=date.today() - timedelta(days=2)), columns=[metric])
  for i in df_count.index:
    df_count_zeros.at[i, metric] = df_count.at[i, metric]
  df_final = df_count_zeros.cumsum().rename(columns={metric: 'cumulative'})
  df_final['single'] = df_count_zeros[metric]
  df_final['rolling'] = df_final['single'].rolling(7, center=True).mean()
  return df_final



#Returns whether a given year is a leap year.
def is_leap_year(year):
  if year % 4 != 0:
    return False
  elif year % 100 == 0:
    if year % 400 == 0:
      return True
    else:
      return False
  else:
    return True



#Annotates the top n names sorted by a pre-determined metric from df in database on ax.
def annotate_top_names(nr, ax, df, n):
  if n == 'all' or n > len(df.index):
    n = len(df.index)
  name_rank_list = nr
  for name in name_rank_list.index[:n]:
    name_index = df.index.get_loc(name)
    ax.annotate(df.index[name_index], ((df.iloc[name_index, :].first_valid_index() - df.columns[0]).days - 20, name_index + 0.5), annotation_clip=False, ha='right', va='center')



#EXPLORATION/DATA METHODS
def cumulative_analysis(database='fatal_encounters'):
  if database == 'fatal_encounters':
    df = pd.read_csv('https://docs.google.com/spreadsheets/d/1dKmaV_JiWcG8XBoRgP8b4e9Eopkpgt7FL7nyspvzAsE/export?format=csv&gid=0').rename(columns={'Date of injury resulting in death (month/day/year)':'date'}).iloc[:-1, :]
  elif database == 'wapo':
    df = pd.read_csv('https://github.com/washingtonpost/data-police-shootings/releases/download/v0.1/fatal-police-shootings-data.csv')
  df['date'] = pd.to_datetime(df['date'], infer_datetime_format=True)
  #df = df.loc[df['Race with imputations'] == 'African-American/Black']
  df = df.loc[df['race'] == 'B']
  df = cumulative(df, 'date', date(2015, 1, 1))
  for i in df.index:
    df.at[i, 'year'] = i.year
  df = df.groupby(['year']).sum()
  print(df)

  nr = pd.read_csv('say_their_names/combined_curated_name_rank.csv')
  for i in ['name_start_date', 'min_rank_date']:
    nr[i] = pd.to_datetime(nr[i], infer_datetime_format=True)
  nr_attention = nr.loc[nr['min_rank'] > 0]
  nr = cumulative(nr, 'name_start_date', date(2009, 1, 1))
  nr_attention = cumulative(nr_attention, 'name_start_date', date(2009, 1, 1))
  """
  plt.subplot(211)
  plt.hist2d(df.index.to_numpy(), df['single'], bins=(30, np.arange(-0.5, df['single'].max() + 1, 1)), cmap='magma_r')
  plt.ylabel('Police-involved deaths per day')
  plt.title('Counts of police-involved deaths per day')
  plt.colorbar(cax=plt.axes([0.91, 0.53, 0.02, 0.35]))
  plt.subplot(212)
  """

  plt.plot(df['single'])
  """
  #plt.scatter(df.index, df['single'], s=2, c='0.5', alpha=0.2)
  #plt.plot(nr_attention['rolling'] / nr['rolling'])
  #plt.plot(nr['rolling'])
  plt.plot(nr_attention['single'])
  plt.xlabel('Date')
  #plt.xlim(left=df.index[0], right=df.index[-1])
  plt.ylabel('Proportion of all names that reach top million rank')
  #plt.ylim(bottom=-0.5, top=df['single'].max() + 0.5)
  #plt.title('Police-involved deaths per day, 30-day rolling average')
  """
  plt.show()



def vertical_analysis(database='combined_curated'):
  df = pd.read_csv(f'say_their_names/{database}.csv').set_index('Unnamed: 0').T
  df.index = pd.to_datetime(df.index, infer_datetime_format=True)
  date_df = pd.DataFrame(index=df.index, columns=['num_mentioned', 'total', 'prop_mentioned', 'single', 'names'])
  nr = pd.read_csv('say_their_names/combined_curated_name_rank.csv')
  nr['name_start_date'] = pd.to_datetime(nr['name_start_date'], infer_datetime_format=True)
  nr = cumulative(nr, 'name_start_date', date(2009, 1, 1))
  for i in df.index:
    #some = len(list(filter(lambda x: x > 0, list(df.loc[i, :]))))
    names = list(filter(lambda x: df.at[i, x] > 0, list(df.columns)))
    some = len(names)
    date_df.loc[i, :] = [some, nr.at[i, 'cumulative'], some / nr.at[i, 'cumulative'], nr.at[i, 'single'], names]
  date_df.to_csv('say_their_names/attention_daily.csv')
  plt.plot(date_df['num_mentioned'], 'b', alpha=0.5)
  plt.plot(date_df['num_mentioned'].rolling(28, center=True).mean(), 'b')
  plt.show()
  plt.plot(date_df['prop_mentioned'], 'g', alpha=0.5)
  plt.plot(date_df['prop_mentioned'].rolling(28, center=True).mean(), 'g')
  plt.show()
  plt.hist(date_df['num_mentioned'], bins=range(0, max(date_df['num_mentioned']) + 3, 2))
  plt.show()



#Creates figures comparing...
def attention_decay(database='combined_curated', days=100, mode='incident', metric='freq_norm', gender='All', top=None, log=False):
  """
  #Read in names
  people = pd.read_csv(f'say_their_names/{database}.csv').set_index('Unnamed: 0').sort_index()
  name_rank = pd.read_csv(f'say_their_names/{database}_name_rank.csv').set_index('Unnamed: 0').sort_index()
  if gender != 'All':
    people = people.loc[name_rank['gender'] == gender]
  people = people.drop(columns=['gender'])
  """
  #Initialize mean plot and hist plot
  if mode == 'incident':
    mean_plot = pd.DataFrame(0, index=range(days + 1), columns=[])
  elif mode == 'peak':
    mean_plot = pd.DataFrame(0, index=range(-10, days + 1), columns=[])
  elif mode == 'absolute':
    mean_plot = pd.DataFrame(0, index=pd.date_range(date(2009, 1, 1), date.today() - timedelta(days=2)), columns=[])
    if days == 0:
      days = len(mean_plot.index)
    mean_plot = mean_plot.iloc[0 - days:, :]
  hist_plot = pd.DataFrame()
  """
  if top:
    name_rank = name_rank.sort_values(by='min_rank').iloc[:top, :]

  for name in people.index:
    if top and not (name in list(name_rank['Unnamed: 0'])):
      continue
    else:
      name_ngram = pd.read_csv(f'say_their_names/{database}/{name}.csv').set_index('time')
      name_ngram.index = pd.to_datetime(name_ngram.index, infer_datetime_format=True)
      if mode == 'absolute':
        name_ngram = name_ngram.iloc[0 - days:, :]
      name_start_date = name_ngram.index[0]
      name_end_date = name_ngram.index[-1]

      #If incident mode, restrict from day of incident to specified number of days after incident
      if mode == 'incident':
        name_ngram = name_ngram.loc[:min(name_start_date + timedelta(days=days), name_end_date),:]
        name_ngram['days_after'] = (name_ngram.index - name_start_date).days
        name_ngram = name_ngram.set_index('days_after')

      #If peak mode, restrict from 10 days before peak (or day of incident if that's later) to specified number of days after peak
      elif mode == 'peak':
        name_ngram = name_ngram.loc[max(name_ngram['freq'].idxmax() - timedelta(days=10), name_start_date):min(name_ngram['freq'].idxmax() + timedelta(days=days), name_end_date), :]
        name_ngram['days_after'] = (name_ngram.index - name_ngram['freq'].idxmax()).days
        name_ngram = name_ngram.set_index('days_after')

      if(name_ngram['rank'].first_valid_index() != None):
        name_ngram[metric] = name_ngram[metric].fillna(0)
        #Plot normalized individual name frequency time series
        plt.scatter(name_ngram.index, name_ngram[metric], s=10, c='0.5', alpha=0.2)
        #Fill mean plot with individual data
        mean_plot[name] = name_ngram[metric]
        #Fill hist plot with individual data
        hist_plot = pd.concat([hist_plot, name_ngram[metric]])

  #Plot mean and display everything
  mean_plot['mean'] = mean_plot.mean(axis=1)
  plt.plot(mean_plot['mean'], 'r', linewidth=1)
  if log:
    plt.yscale('log')
  #plt.ylim(2 * 10**-4, 1.5)
  
  plt.show()
  """
  hist_plot = pd.read_csv('hist_plot.csv').set_index('Unnamed: 0')
  hist_plot.index = pd.to_datetime(hist_plot.index, infer_datetime_format=True)

  #Plot 2D histogram
  fig = plt.figure(figsize=[12, 7])
  ax1 = plt.axes([0.1, 0.07, 0.8, 0.92])
  ax2 = plt.axes([0.92, 0.07, 0.02, 0.92])
  if mode == 'peak':
    days += 10
  bins = [100, int(days/30)] if mode == 'absolute' else [100, days]
  hist_plot.index = dates.date2num(hist_plot.index)
  print(np.nanmax(np.nanmax(ax1.hist2d(hist_plot.iloc[:, 0], hist_plot.index.to_numpy(), bins=bins, range=[[10**-10, 1], [min(hist_plot.index), max(hist_plot.index)]], cmin=1, cmax=481, cmap=colormap('2d_hist_scaled'))[0])))
  ax1.set_ylabel('Date', fontsize=15)
  ax1.set_xlabel('Normalized frequency', fontsize=15)

  ax1.yaxis.grid(color='0', alpha=0.4)

  majorticklocations = [dates.date2num(date(2009, 1, 1))]
  minorticklocations = []
  ticklabels = []
  tickday = dates.date2num(date(2009, 1, 1))
  for year in range(2009, 2021):
    ticklabels.append(year)
    if is_leap_year(year):
      tickday += 1
    minorticklocations.extend([tickday + 90, tickday + 181, tickday + 273])
    tickday += 365
    majorticklocations.append(tickday)
  ticklabels.append(2021)
  if date.today().month > 3:
    minorticklocations.append(tickday + 90)
    if date.today().month > 6:
      minorticklocations.append(tickday + 181)
      if date.today().month > 9:
        minorticklocations.append(tickday + 273)
  ax1.set_yticks(majorticklocations)
  ax1.set_yticklabels(ticklabels, rotation=0)
  ax1.yaxis.set_minor_locator(ticker.FixedLocator(minorticklocations))

  ticks = np.linspace(1/8, 7/8, 7)
  labels = ['1', '5', '10', '25', '50', '75', '100']
  fig.colorbar(cm.ScalarMappable(cmap=colormap('2d_hist_unscaled')), cax=ax2, ticks=ticks)
  ax2.set_yticklabels(labels)
  ax2.set_ylabel('Count', fontsize=15, rotation=270)
  ax2.yaxis.set_label_coords(3, 0.5)
  plt.show()
  #fig.savefig('say_their_names/figures/decay/2dhist.pdf', bbox_inches='tight')
  
  


#Examine trends in minimum rank
def min_rank_graph(database='combined_curated', gender='All'):
  def func(x, a, b, c):
    return a / (x + b) + c
  
  def func2(x, b, m):
    return 10**b * x**m

  h = plt.figure()
  """
  plt.subplot(121)
  shootings_f = pd.read_csv('say_their_names/combined_curated_name_rank.csv').dropna(how='any').sort_values(by='max_freq', ascending=False, ignore_index=True)
  shootings_f.index = shootings_f.index + 1
  popt, _ = optimize.curve_fit(func, shootings_f.index, shootings_f['max_freq'])
  print(f'{popt[0]} / (x + {popt[1]}) + {popt[2]}')
  plt.plot(shootings_f.index, func(shootings_f.index, *popt))
  plt.plot(shootings_f.index, shootings_f['max_freq'])
  plt.ylabel('Maximum freq')
  plt.xlabel('Rank of name by maximum freq')
  plt.xscale('log')
  plt.yscale('log')
  plt.annotate(r'$y=\dfrac{8.24\times10^{-4}}{x-0.229}-3.588\times10^{-7}$', (1, 10**-6))
  
  #The lowest min. ranks exhibit a squared pattern
  plt.subplot(122)
  """
  shootings_r = pd.read_csv('say_their_names/combined_curated_name_rank.csv')
  shootings_r = shootings_r.loc[shootings_r['min_rank'] == shootings_r['min_rank']]
  if gender != 'All':
    shootings_r = shootings_r.loc[shootings_r['gender'] == gender]
  shootings_r = shootings_r.sort_values(by='min_rank', ignore_index=True)
  shootings_r.index = shootings_r.index + 1
  """
  mymodel = np.poly1d(np.polyfit(shootings_r.index, shootings_r['min_rank'], 2))
  print(mymodel)
  myline = np.linspace(1, len(shootings_r.index) - 1, len(shootings_r.index) - 1)
  plt.plot(myline, mymodel(myline), lw=0.8)
  """
  popt, _ = optimize.curve_fit(func2, shootings_r.index, shootings_r['min_rank'])
  print(f'10^{popt[0]} * x^{popt[1]}')
  plt.scatter(shootings_r.index, shootings_r['min_rank'], s=1, c='0.5', label='Names with measurable attention')
  plt.plot(shootings_r.index, func2(shootings_r.index, *popt), label=r'$y=10^{-0.344} * x^{2.076}$')
  plt.ylabel('Minimum rank', fontsize=15)
  plt.xlabel('Rank of name by minimum rank', fontsize=15)
  #plt.xscale('log')
  #plt.yscale('log')
  plt.gca().invert_yaxis()
  plt.legend()
  plt.show()
  h.savefig('say_their_names/figures/min_rank_trend.pdf', bbox_inches='tight')
  
  #Number of names that hit their min. rank per day
  df = shootings_r['min_rank_date'].value_counts().to_frame().sort_index()
  df.index = pd.to_datetime(df.index, infer_datetime_format=True)
  df_final = pd.DataFrame(index=pd.date_range(start='1/1/2009', end=df.index[-1]), columns=['count'])
  df_final['count'] = 0
  for i in df.index:
    df_final.at[i, 'count'] = df.at[i, 'min_rank_date']
  print(df_final.sort_values(by='count'))
  plt.plot(df_final.index, df_final['count'], lw=0.5)
  plt.show()



def name_rank_updates(database, window=7):
  people = pd.read_csv(f'say_their_names/{database}.csv').set_index('Unnamed: 0')
  name_rank = pd.DataFrame()

  for name in people.index:
    name_ngram = pd.read_csv(f'say_their_names/{database}/{name}.csv').set_index('time')
    name_ngram.index = pd.to_datetime(name_ngram.index, infer_datetime_format=True)

    for i in range(len(DATES)):
      name_rank.at[name, f'mean_freq_{str(i)}_before'] = name_ngram.loc[DATES[i][0] - timedelta(days=window):DATES[i][0] - timedelta(days=1), 'freq'].mean()
      name_rank.at[name, f'mean_freq_{str(i)}_during'] = name_ngram.loc[DATES[i][0]:DATES[i][1], 'freq'].mean()
      name_rank.at[name, f'mean_freq_{str(i)}_after'] = name_ngram.loc[DATES[i][1] + timedelta(days=1):DATES[i][1] + timedelta(days=window), 'freq'].mean()
    name_rank.at[name, 'mean_freq_initial'] = name_ngram.iloc[:math.floor(window/2) - 1]['freq'].mean() if len(name_ngram.index) >= math.floor(window/2) else float('nan')
    name_rank.at[name, 'name_start_date'] = name_ngram.index[0]
    name_rank.at[name, 'gender'] = people.at[name, 'gender']
    name_rank.at[name, 'min_rank'] = name_ngram['rank'].min()
    if name_ngram['rank'].idxmin() == name_ngram['rank'].idxmin():
      name_rank.at[name, 'min_rank_date'] = name_ngram['rank'].idxmin()
    name_rank.at[name, 'max_freq'] = name_ngram['freq'].max()
    #name_rank.at[name, 'sum_freq'] = name_ngram['freq'].sum()
    name_rank.at[name, 'mean_freq'] = name_ngram['freq'].mean()
    name_rank.at[name, 'mean_freq_norm'] = name_ngram['freq_norm'].mean()
    name_rank.at[name, 'num_days_mentioned'] = name_ngram['rank'].count()
    name_rank.at[name, 'prop_days_mentioned'] = name_ngram['rank'].count()/len(name_ngram.index)
    if name_ngram['rank'].idxmin() == name_ngram['rank'].idxmin():
      name_rank.at[name, 'peak_delay'] = (name_rank.at[name, 'min_rank_date'] - name_rank.at[name, 'name_start_date']).days

  name_rank.to_csv(f'say_their_names/{database}_name_rank.csv')



#Make plots comparing a variety of name rank metrics.
def name_rank_plots(database='combined_curated', overwrite=False, gender='All'):
  if overwrite:
    name_rank_updates(database)
  
  name_rank = pd.read_csv(f'say_their_names/{database}_name_rank.csv').set_index('Unnamed: 0')
  for column in ['name_start_date', 'min_rank_date']:
    name_rank[column] = pd.to_datetime(name_rank[column], infer_datetime_format=True)
  if gender != 'All':
    name_rank = name_rank.loc[name_rank['gender'] == gender]
  
  if 'combined' in database:
    #Histograms
    #metrics = name_rank.columns
    metrics = ['prop_days_mentioned', 'min_rank', 'peak_delay']
    subplots = len(metrics)
    
    for i in range(2, 3):#len(metrics)):
      #plt.subplot(1, len(metrics), i + 1)
      #name_rank[metrics[i]].plot.kde()
      plt.hist(name_rank[metrics[i]], bins=range(0, 100))
      plt.xlabel(metrics[i])
    plt.show()
    
    subplots = len(metrics) * (len(metrics) - 1) / 2
    rows = 0
    columns = 0
    for i in range(math.floor(math.sqrt(subplots)), 0, -1):
      if subplots % i == 0:
        columns = int(subplots / i)
        rows = i
        break
    
    f = plt.figure(figsize=[12, 7])
    count = 0
    for i in range(len(metrics) - 1):
      for j in range(i + 1, len(metrics)):
        count += 1
        plt.subplot(rows, columns, count)
        x = i
        y = j
        if metrics[i] == 'mean_freq' or metrics[j] == 'min_rank':
          x = j
          y = i
        if metrics[i] == 'name_start_date' or metrics[i] == 'min_rank_date':
          x = i
          y = j
        elif metrics[j] == 'name_start_date' or metrics[j] == 'min_rank_date':
          x = j
          y = i
        plt.scatter(name_rank[metrics[x]], name_rank[metrics[y]], 10, name_rank['name_start_date'], cmap=colormap('name_rank'))
        #plt.annotate(metrics[(j + 1) % len(metrics)], (0, 0), xycoords = 'axes points')
        names_male = {
          'George Floyd':(-40, -3),
          'Jacob Blake':(0, -3),
          'Alton Sterling':(0, -3),
          'David McAtee':(0, -3),
          'Terence Crutcher':(3, 0),
          'David Long':(0, -3),
          'Antonio Martin':(0, -3),
          'Aaron Campbell':(0, -3),
          'Darius Smith':(0, -3),
          'Bobby Gross':(3, 0),
          'Donald Ivy':(0, -3),
          'Garfield King':(0, 10),
          #'Trenton Booker':(0, -3),
          'Trayvon Martin':(-55, 12),
          'Michael Brown':(-55, 12),
          'Tamir Rice':(-56, 5),
          'Walter Scott':(-40, 12),
          'Casey Goodson':(-35, 12),
          #'John McAfee':(-63, 8),
          'Andre Hill':(-30, 12),
          #'Kevin Maguire':(-70, 12),
          'Patrick Warren':(-60, 12),
          'Sincere Pierce':(-70, 12),
          #'Majok Nyariel':(-58, 12),
          #'Rayshard Brooks':(40, -15),
          #'Eric Garner':(30, -12),
          #'Philando Castile':(10, -10),
          #'Freddie Gray':(-10, 7),
          #'Walter Wallace':(0, 10),
          #'Oscar Grant':(0, -10),
          #'Ahmaud Arbery':(0, 10),
          #'James Tyson':(0, 10),
          #'Ryan Clayton':(10, -10),
          #'Gavin Long':(15, -10),
          #'Michael Clarke':(-20, 8),
          #'Dolal Idd':(5, 10),
          #'Angelo Crooms':(0, 20),
          #'Anthony Jackson':(40, 15),
          #'Marcellis Stinnette':(-10, 10),
          #'Kenneth Jones':(-30, 0),
          #'Bennie Edwards':(-20, 8),
          #'Joshua Feast':(-30, 0),
          #'Elijah McClain':(31, 1),
          #'Stephon Clark':(-5, -10),
          #'Botham Jean':(25, 6),
          #'Jonathan Price':(0, -10),
          #'Laquan McDonald':(-20, -10),
        }
        names_female = {
          'Oluwatoyin Salau':(0, -3),
          'Sandra Bland':(-5, 10),
          'Atatiana Jefferson':(0, 10),
          'Charleena Lyles':(0, -3),
          'Shukri Said':(0, -3),
          'Breonna Taylor':(-19, 12),
          'Rekia Boyd':(0, -3),
          'Miriam Carey':(0, -3),
          'Yvette Smith':(0, -3),
          'Tanisha Anderson':(0, -3),
          'Michelle Cusseaux':(0, -3),
          'Pamela Turner':(0, -3),
          'Dominique Clayton':(0, -3),
          'Janet Wilson':(0, -3),
        }
        if gender == 'All':
          names = {**names_male, **names_female}
        elif gender == 'Male':
          names = names_male
        elif gender == 'Female':
          names = names_female
        for n in list(names.keys()):
          plt.annotate(n, (name_rank.at[n, metrics[x]], name_rank.at[n, metrics[y]]), names.get(n), textcoords='offset points', horizontalalignment='left', verticalalignment='top')
        if metrics[x] != 'peak_delay' and metrics[x] != 'name_start_date' and metrics[x] != 'min_rank_date':
          plt.xscale('log')
        if metrics[y] != 'peak_delay' and metrics[y] != 'name_start_date' and metrics[y] != 'min_rank_date':
          plt.yscale('log')
        #plt.tick_params(which='both', left=False, bottom=False)
        plt.gca().set_xlabel('Peak rank', fontsize=15)
        if metrics[x] == 'min_rank':
          plt.gca().invert_xaxis()
        #plt.gca().set_xticklabels([])
        #plt.gca().xaxis.set_label_coords(0.5, 0)
        plt.gca().set_ylabel('Proportion of days with attention', fontsize=15)
        #plt.gca().set_yticklabels([])
        #plt.gca().yaxis.set_label_coords(0, 0.5)
        plt.gca().set_aspect('equal')
    
    cax = plt.axes([0.85, 0.1, 0.02, 0.8])
    plt.colorbar(cm.ScalarMappable(cmap=colormap('name_rank')), cax=cax, ticks=np.linspace(1/26, 25/26, 13))
    cax.set_yticklabels(range(2009, 2022), rotation=0)
    cax.set_ylabel('Year of incident', rotation=270, fontsize=15)
    cax.yaxis.set_label_coords(4, 0.5)
    plt.show()
    f.savefig(f'say_their_names/figures/name_rank/name_rank_plot_1_{gender}.pdf', bbox_inches='tight')



def spike_analysis(database='combined_curated', gender='All'):
  windows = [7, 30, 60, 90]
  initial = pd.DataFrame(columns=windows)
  for d in windows:
    print(d)
    name_rank_updates(database, d)
    name_rank = pd.read_csv(f'say_their_names/{database}_name_rank.csv').set_index('Unnamed: 0')
    for column in ['name_start_date', 'min_rank_date']:
      name_rank[column] = pd.to_datetime(name_rank[column], infer_datetime_format=True)
    if gender != 'All':
      name_rank = name_rank.loc[name_rank['gender'] == gender]

    temp = name_rank.loc[name_rank['mean_freq_4_during'] > name_rank['mean_freq_4_before'], ['name_start_date', 'mean_freq_4_before', 'mean_freq_4_during']]
    temp.to_csv(f'temp_{d}.csv')

    resurgence = pd.DataFrame(columns=['during - before', 'after - before', 'after - during'])

    for w in range(len(DATES)):
      names_before = name_rank.loc[name_rank['name_start_date'] < DATES[w][0]]
      names_before_window = name_rank.loc[(name_rank['name_start_date'] >= DATES[w][0] - timedelta(days=d)) & (name_rank['name_start_date'] < DATES[w][0] - timedelta(days=math.floor(d/2)))]
      print(names_before_window['name_start_date'])
      names_after_window = name_rank.loc[(name_rank['name_start_date'] > DATES[w][1]) & (name_rank['name_start_date'] <= DATES[w][1] + timedelta(days=d))]
      print(names_after_window['name_start_date'])
      temp_0 = DATES[w][0].strftime('%-m/%-d/%Y')
      temp_1 = DATES[w][1].strftime('%-m/%-d/%Y')
      resurgence.loc[f'{temp_0} to {temp_1}', :] = [
        (names_before[f'mean_freq_{w}_during'] - names_before[f'mean_freq_{w}_before']).mean(),
        (names_before[f'mean_freq_{w}_after'] - names_before[f'mean_freq_{w}_before']).mean(),
        (names_before[f'mean_freq_{w}_after'] - names_before[f'mean_freq_{w}_during']).mean()]
      initial.at[f'{temp_0} to {temp_1}', d] = names_after_window['mean_freq_initial'].mean() - names_before_window['mean_freq_initial'].mean()
    print(resurgence)
    print()
    
  print('\n\n')
  print(initial)



def name_length(database='combined_curated', gender='All'):
  name_rank = pd.read_csv(f'say_their_names/{database}_name_rank.csv').set_index('Unnamed: 0')
  for column in ['name_start_date', 'min_rank_date']:
    name_rank[column] = pd.to_datetime(name_rank[column], infer_datetime_format=True)
  if gender != 'All':
    name_rank = name_rank.loc[name_rank['gender'] == gender]
  
  na = name_rank.loc[name_rank['min_rank'] != name_rank['min_rank']]
  a = name_rank.loc[name_rank['min_rank'] == name_rank['min_rank']]
  for df in [na, a]:
    for row in df.index:
      df.at[row, 'letters'] = len(row)
  print(na['letters'].mean(), a['letters'].mean())
  print(stats.ttest_ind(na['letters'], a['letters']))
  g = plt.figure()
  plt.subplot(211)
  plt.hist(na['letters'], bins=range(7, 29))
  plt.ylabel('No measurable attention', fontsize=12)
  plt.subplot(212)
  plt.hist(a['letters'], bins=range(7, 29))
  plt.xlabel('Characters in name', fontsize=15)
  plt.ylabel('Measurable attention', fontsize=12)
  plt.annotate('Count', (0.04, 0.5), xycoords='figure fraction', fontsize=15, rotation=90, ha='center', va='center')
  plt.show()
  g.savefig('say_their_names/figures/name_length.pdf', bbox_inches='tight')



#Combine Fatal Encounters and manual databases
def combine_databases(curated=True):
  fatal_encounters = pd.read_csv('https://docs.google.com/spreadsheets/d/1dKmaV_JiWcG8XBoRgP8b4e9Eopkpgt7FL7nyspvzAsE/export?format=csv&gid=0')[['Unique ID', 'Name', 'Gender', 'Race with imputations', 'Date of injury resulting in death (month/day/year)', 'Highest level of force', 'Intended use of force (Developing)']]
  fatal_encounters.rename(columns={'Name':'name', 'Gender':'gender', 'Unique ID':'id', 'Race with imputations':'race', 'Date of injury resulting in death (month/day/year)':'date', 'Highest level of force':'highest_force', 'Intended use of force (Developing)':'intended_force'}, inplace=True)
  fatal_encounters = fatal_encounters.loc[fatal_encounters['race'] == 'African-American/Black'].drop(columns=['race'])
  if curated:
    fatal_encounters = fatal_encounters.loc[fatal_encounters['highest_force'] != 'Vehicle']
    fatal_encounters = fatal_encounters.loc[fatal_encounters['intended_force'] != 'Suicide']
    fatal_encounters = fatal_encounters.loc[fatal_encounters['intended_force'] != 'Pursuit']
  
  manual = pd.read_csv('say_their_names/MANUAL_RAW.csv')

  for df in [fatal_encounters, manual]:
    df['date'] = pd.to_datetime(df['date'], infer_datetime_format=True)

  combined = pd.concat([fatal_encounters, manual], ignore_index=True).sort_values(by='date', ignore_index=True)
  if curated:
    combined.to_csv('say_their_names/COMBINED_CURATED_RAW.csv')
  else:
    combined.to_csv('say_their_names/COMBINED_RAW.csv')



#Create main figure
def heatmap(database='manual', overwrite=False, scaled=True, metric='min_rank', top=None, annotate=25, dropna=True):
  if overwrite:
    lang = storywrangler.get_lang('en')
    #Read in raw data
    if database == 'fatal_encounters':
      start_date = date(2008, 12, 22)
      df = pd.read_csv('https://docs.google.com/spreadsheets/d/1dKmaV_JiWcG8XBoRgP8b4e9Eopkpgt7FL7nyspvzAsE/export?format=csv&gid=0')[['Unique ID', 'Name', 'Race with imputations', 'Date of injury resulting in death (month/day/year)']]
      df = df.rename(columns={'Name': 'name', 'Unique ID':'id', 'Date of injury resulting in death (month/day/year)':'date'})
      df = df.loc[df['Race with imputations'] == 'African-American/Black']
      df['date'] = pd.to_datetime(df['date'], infer_datetime_format=True)
      df = df.loc[df['date'] >= datetime(start_date.year, start_date.month, start_date.day) + timedelta(days=10)]
    elif database == 'vehicle':
      start_date = date(2008, 12, 22)
      df = pd.read_csv('https://docs.google.com/spreadsheets/d/1dKmaV_JiWcG8XBoRgP8b4e9Eopkpgt7FL7nyspvzAsE/export?format=csv&gid=0')[['Unique ID', 'Name', 'Race with imputations', 'Date of injury resulting in death (month/day/year)', 'Highest level of force']]
      df = df.rename(columns={'Name': 'name', 'Unique ID':'id', 'Date of injury resulting in death (month/day/year)':'date', 'Highest level of force':'force'})
      df = df.loc[df['Race with imputations'] == 'African-American/Black']
      df = df.loc[df['force'] == 'Vehicle']
      df['date'] = pd.to_datetime(df['date'], infer_datetime_format=True)
      df = df.loc[df['date'] >= datetime(start_date.year, start_date.month, start_date.day) + timedelta(days=10)]
    elif database == 'wapo':
      start_date = date(2014, 12, 22)
      df = pd.read_csv('https://github.com/washingtonpost/data-police-shootings/releases/download/v0.1/fatal-police-shootings-data.csv')
      df = df.loc[df.race == 'B']
      df['date'] = pd.to_datetime(df['date'], infer_datetime_format=True)
    elif database == 'manual':
      start_date = date(2008, 12, 22)
      df = pd.read_csv('say_their_names/MANUAL_RAW.csv')
      df['date'] = pd.to_datetime(df['date'], infer_datetime_format=True)
    elif database == 'popular':
      start_date = date(2008, 12, 22)
      df = pd.read_csv('say_their_names/POPULAR_RAW.csv')
      df['date'] = pd.to_datetime(df['date'], infer_datetime_format=True)
    elif database == 'combined':
      combine_databases()
      start_date = date(2008, 12, 22)
      df = pd.read_csv('say_their_names/COMBINED_RAW.csv')
      df['date'] = pd.to_datetime(df['date'], infer_datetime_format=True)
      df = df.loc[df['date'] >= datetime(start_date.year, start_date.month, start_date.day) + timedelta(days=10)]
    elif database == 'combined_curated':
      combine_databases(curated=True)
      start_date = date(2008, 12, 22)
      df = pd.read_csv('say_their_names/COMBINED_CURATED_RAW.csv')
      df['date'] = pd.to_datetime(df['date'], infer_datetime_format=True)
      df = df.loc[df['date'] >= datetime(start_date.year, start_date.month, start_date.day) + timedelta(days=10)]
    else:
      print('Invalid database!')
      return
    
    DUPLICATES = 0
    ANOMALIES = 0

    #Initialize heatmap and ratio DataFrames
    heatmap = pd.DataFrame(columns=pd.date_range(start=start_date + timedelta(days=10), end=date.today() - timedelta(days=2)))
    heatmap.insert(0, 'gender', [])
    ratios = pd.DataFrame(columns=pd.date_range(start=start_date + timedelta(days=10), end=date.today() - timedelta(days=2)))
    anomaly_names = []

    #For each name in the database of Black victims of fatal police shootings:
    for i in df.index:
      #Format names
      formatted_name = parse_name(df.at[i, 'name'])

      if formatted_name not in heatmap.index and formatted_name not in ['Thomas Lane', 'Darren Wilson', '_UNKNOWN_NAME_']:
        #Get ngram
        name_start_date = df.at[i, 'date'] - timedelta(days=10)
        name_ngram = storywrangler.get_ngram(formatted_name, 'en', name_start_date)
        
        if name_ngram['freq'].first_valid_index() and formatted_name not in ['Emmett Till', 'Rodney King', 'Amadou Diallo', 'Sean Bell', 'Walter Scott']:
          if (name_ngram['freq'].first_valid_index() - name_ngram.index[0]).days < 10:
            anomaly_names.append(formatted_name)
            ANOMALIES += 1
            continue
        
        if (name_ngram.index[0] < datetime(2008, 12, 22)):
          name_ngram = name_ngram.loc['2008-12-22':, :]
        name_ngram = name_ngram.iloc[10:, :]

        #Normalize and scale frequency
        name_ngram['freq'] = name_ngram['freq'].fillna(0)
        name_ngram['freq_norm'] = name_ngram['freq'] / name_ngram['freq'].max()
        
        heatmap.at[formatted_name, 'gender'] = df.at[i, 'gender']
        heatmap.iloc[heatmap.index.get_loc(formatted_name), 1:] = name_ngram['freq_norm']

        #calculate relative amplification
        name_ngram['lang_count'] = lang['count']
        name_ngram['lang_count_no_rt'] = lang['count_no_rt']

        name_ngram['lang_num_2grams'] = lang['num_2grams']
        name_ngram['lang_num_2grams_no_rt'] = lang['num_2grams_no_rt']

        ratio = (name_ngram['count'] - name_ngram['count_no_rt']) / name_ngram['count']
        lang_ratio = (name_ngram['lang_num_2grams'] - name_ngram['lang_num_2grams_no_rt']
                    ) / name_ngram['lang_num_2grams']

        name_ngram['r_rel'] = ratio / lang_ratio
        name_ngram['r_rel'] = name_ngram['r_rel'].replace([np.inf, -np.inf, np.nan], 1)

        ratios.loc[formatted_name, :] = name_ngram['r_rel']

        name_ngram.to_csv(f'say_their_names/{database}/{formatted_name}.csv')

      else:
        DUPLICATES += 1

    heatmap = heatmap.loc[::-1]

    #Save DataFrame and files
    heatmap.to_csv(f'say_their_names/{database}.csv')
    ratios.to_csv(f'say_their_names/{database}_ratios.csv')
    json.dump(anomaly_names, open(f'say_their_names/{database}_anomalies', 'w'))
    name_rank_updates(database)

    print(f'Duplicates and manual removals: {DUPLICATES}')
    print(f'Anomalies: {ANOMALIES}')
  
  #Load DataFrame
  heatmap = pd.read_csv(f'say_their_names/{database}.csv').rename(columns={'Unnamed: 0': 'Name'})
  heatmap = heatmap.drop(columns=['gender'])
  heatmap = heatmap.set_index('Name')
  heatmap.columns = pd.to_datetime(heatmap.columns, infer_datetime_format=True)

  
  if dropna:
    heatmap = heatmap.dropna(how='all')

  if metric:
    name_rank = pd.read_csv(f'say_their_names/{database}_name_rank.csv').set_index('Unnamed: 0')
    name_rank = name_rank.sort_values(by=[metric]) if metric in (['name_start_date', 'min_rank', 'min_rank_date', 'peak_delay']) else name_rank.sort_values(by=[metric], ascending=False)
    if top:
      name_rank = name_rank.iloc[:top, :]
    heatmap = heatmap.loc[heatmap.index.isin(name_rank.index)]
  
  #Set graph and colorbar dimensions
  fig = plt.figure(figsize=[12, 7])
  grid_color = '0.7'
  grid_alpha = 1
  plt.rcParams['axes.facecolor'] = '0.7'
  ax1 = plt.axes([0.1, 0.07, 0.8, 0.87])
  ax3 = plt.axes([0.1, 0.94, 0.8, 0.05])
  ax2 = plt.axes([0.92, 0.07, 0.02, 0.92])

  #Plot main heatmap
  if scaled:
    ticks = [0.025, 0.23, 0.41, 0.59, 0.77, 0.975]
    labels = ['0', '1/100', '1/30', '1/10', '1/3', '1']
    fig.colorbar(cm.ScalarMappable(cmap=colormap('heatmap_scaled_display')), cax=ax2, ticks=ticks)
    ax2.set_yticklabels(labels)
    ax1_fig = sns.heatmap(data=heatmap, vmin=0, vmax=1, cmap=colormap('heatmap_scaled'), ax=ax1, cbar=False)
  else:
    ax1_fig = sns.heatmap(data=heatmap, vmin=0, vmax=1, cmap=colormap('heatmap_unscaled'), ax=ax1, cbar_ax=ax2)
  ax2.set_ylabel('Normalized frequency', rotation=270, fontsize=15)
  ax2.yaxis.set_label_coords(4, 0.5)
  annotate_top_names(name_rank, ax1, heatmap, annotate)
  
  for _, spine in ax1_fig.spines.items():
    spine.set_visible(True)
  
  majorticklocations = [0]
  minorticklocations = []
  ticklabels = []
  tickday = 0
  for year in range(heatmap.columns[0].year, heatmap.columns[-1].year):
    ticklabels.append(year)
    if is_leap_year(year):
      tickday += 1
    minorticklocations.extend([tickday + 90, tickday + 181, tickday + 273])
    tickday += 365
    majorticklocations.append(tickday)
  ticklabels.append(heatmap.columns[-1].year)
  if date.today().month > 3:
    minorticklocations.append(tickday + 90)
    if date.today().month > 6:
      minorticklocations.append(tickday + 181)
      if date.today().month > 9:
        minorticklocations.append(tickday + 273)
  ax1.set_xticks(majorticklocations)
  ax1.set_xticklabels(ticklabels, rotation=0)
  ax1.xaxis.set_minor_locator(ticker.FixedLocator(minorticklocations))

  ax1.xaxis.grid(color=grid_color, alpha=grid_alpha)
  ax1.set_xlabel('Date', fontsize=15)
  ax1.yaxis.set_visible(False)
  

  #Plot heatmap for other terms at top
  terms = pd.DataFrame(0, index=pd.date_range(start=date(2009, 1, 1), end=date(heatmap.columns[-1].year, heatmap.columns[-1].month, heatmap.columns[-1].day)), columns=[])
  blm = storywrangler.get_ngram('Black Lives Matter', 'en', datetime(2014, 10, 14))
  blm['freq'] = blm['freq'].fillna(0)
  terms['Black Lives Matter'] = blm['freq'] / blm['freq'].max()
  blm_hashtag = storywrangler.get_ngram('#BlackLivesMatter', 'en', datetime(2013, 7, 17))
  blm_hashtag['freq'] = blm_hashtag['freq'].fillna(0)
  terms['#BlackLivesMatter'] = blm_hashtag['freq'] / blm_hashtag['freq'].max()
  terms = terms.T
  ax3_fig = sns.heatmap(data=terms, vmin=0, vmax=1, cmap=colormap('heatmap_scaled'), ax=ax3, cbar=False) if scaled else sns.heatmap(data=terms, vmin=0, vmax=1, cmap=colormap('heatmap_unscaled'), ax=ax3, cbar=False)
  
  for i in range(len(terms.index)):
    ax3.annotate(f'"{terms.index[i]}"', ((terms.iloc[i, :].first_valid_index() - terms.columns[0]).days - 20, i + 0.5), ha='right', va='center')

  for _, spine in ax3_fig.spines.items():
    spine.set_visible(True)
  
  ax3.set_xticks(majorticklocations)
  ax3.set_xticklabels([])
  ax3.xaxis.grid(color=grid_color, alpha=grid_alpha)
  ax3.tick_params(axis='x', colors=[0, 0, 0, 0])
  
  ax3.yaxis.set_visible(False)
  
  for d in DATES:
    ax1.axvspan((d[0] - datetime(2009, 1, 1)).days, (d[1] - datetime(2009, 1, 1)).days, edgecolor=[0, 0, 0, 0], facecolor=[1, 1, 1, 0.5], zorder=0)
    ax3.axvspan((d[0] - datetime(2009, 1, 1)).days, (d[1] - datetime(2009, 1, 1)).days, edgecolor=[0, 0, 0, 0], facecolor=[1, 1, 1, 0.5], zorder=0)

  plt.show()
  fig.savefig(f'say_their_names/figures/heatmap/{database}_{top}_{annotate}.pdf', bbox_inches='tight')



def ratios(database='combined_curated'):
  ratios = pd.read_csv(f'say_their_names/{database}_ratios.csv').set_index('Unnamed: 0').sort_index()
  ratios.columns = pd.to_datetime(ratios.columns, infer_datetime_format=True)
  """
  name_rank = pd.read_csv(f'say_their_names/{database}_name_rank.csv').set_index('Unnamed: 0').sort_index()
  props_amplified = pd.DataFrame()
  for name in ratios.index:
    props_amplified.at[name, 'prop_amplified'] = len(list(filter(lambda x: x > 1, ratios.loc[name, :]))) / len(list(filter(lambda x: x == x, ratios.loc[name, :])))
  props_amplified.to_csv(f'say_their_names/{database}_ratios_props_amplified.csv')
  props_amplified = pd.read_csv(f'say_their_names/{database}_ratios_props_amplified.csv').set_index('Unnamed: 0').sort_index()
  
  props_amplified_male = props_amplified.loc[name_rank['gender'] == 'Male']
  props_amplified_female = props_amplified.loc[name_rank['gender'] == 'Female']
  plt.subplot(1, 3, 1)
  plt.hist(props_amplified['prop_amplified'], bins=np.linspace(0, 0.65, 66))
  plt.yscale('log')
  plt.xlabel('All')
  plt.subplot(1, 3, 2)
  plt.hist(props_amplified_male['prop_amplified'], bins=np.linspace(0, 0.65, 66))
  plt.yscale('log')
  plt.xlabel('Male')
  plt.subplot(1, 3, 3)
  plt.hist(props_amplified_female['prop_amplified'], bins=np.linspace(0, 0.65, 66))
  plt.yscale('log')
  plt.xlabel('Female')
  plt.show()
  
  plt.scatter(ratios.mean(axis=1), props_amplified['prop_amplified'])
  plt.xlabel('Mean R_rel')
  plt.ylabel('Proportion of days amplified')
  #plt.xscale('log')
  #plt.yscale('log')
  #plt.gca().invert_xaxis()
  #plt.ylim(10**-4, 1.01)
  #plt.xlim(10**-4, 1.01)
  names = {
    'Trayvon Martin':(0, 0),
    'George Floyd':(0, 0),
    'Walter Scott':(0, 0),
    'Jacob Blake':(0, 0),
    'Alton Sterling':(0, 0),
    'David McAtee':(0, 0),
    'Terence Crutcher':(0, 0),
    'David Long':(0, 0),
    'Antonio Martin':(0, 0),
    'Aaron Campbell':(0, 0),
    'Darius Smith':(0, 0),
    'Bobby Gross':(0, 0),
    'Donald Ivy':(0, 0),
    'Garfield King':(0, 0),
    'Oluwatoyin Salau':(0, 0),
    'Sandra Bland':(0, 0),
    'Atatiana Jefferson':(0, 0),
    'Charleena Lyles':(0, 0),
    'Shukri Said':(0, 0),
    'Breonna Taylor':(0, 0),
    'Rekia Boyd':(0, 0),
    'Miriam Carey':(0, 0),
    'Yvette Smith':(0, 0),
    'Tanisha Anderson':(0, 0),
    'Michelle Cusseaux':(0, 0),
    'Pamela Turner':(0, 0),
    'Dominique Clayton':(0, 0),
    'Janet Wilson':(0, 0),
  }
  #for n in list(names.keys()):
    #plt.annotate(n, (name_rank.at[n, 'min_rank'], ratios.max(axis=1).at[n]), names.get(n), textcoords='offset points', horizontalalignment='left', verticalalignment='top')
  plt.show()
  
  ratios_male = ratios.loc[name_rank['gender'] == 'Male']
  ratios_female = ratios.loc[name_rank['gender'] == 'Female']
  #print(ratios_male.mean(axis=1).sort_values())
  #plt.plot(ratios_male.mean(axis=0), label='Male')
  #print(ratios_female.mean(axis=1).sort_values())
  #plt.plot(ratios_female.mean(axis=0), label='Female')
  #plt.legend()
  #plt.show()
  """

  hist_plot = ratios.stack().droplevel('Unnamed: 0')
  days = len(ratios.columns)

  #Plot 2D histogram
  fig = plt.figure(figsize=[12, 7])
  ax1 = plt.axes([0.1, 0.07, 0.8, 0.92])
  ax2 = plt.axes([0.92, 0.07, 0.02, 0.92])
  bins = [300, int(days/30)]
  hist_plot.index = dates.date2num(hist_plot.index)
  print(np.nanmax(np.nanmax(ax1.hist2d(hist_plot.array, hist_plot.index.to_numpy(), bins=bins, range=[[0, 3], [min(hist_plot.index), max(hist_plot.index)]], cmin=1, cmax=109, cmap=colormap('2d_hist_ratios'))[0])))
  ax1.set_ylabel('Date', fontsize=15)
  ax1.set_xlabel(r'$R^\mathrm{rel}$', fontsize=15)
  ax1.axvline(x=1.005)

  ax1.yaxis.grid(color='0', alpha=0.4)

  majorticklocations = [dates.date2num(date(2009, 1, 1))]
  minorticklocations = []
  ticklabels = []
  tickday = dates.date2num(date(2009, 1, 1))
  for year in range(2009, 2021):
    ticklabels.append(year)
    if is_leap_year(year):
      tickday += 1
    minorticklocations.extend([tickday + 90, tickday + 181, tickday + 273])
    tickday += 365
    majorticklocations.append(tickday)
  ticklabels.append(2021)
  if date.today().month > 3:
    minorticklocations.append(tickday + 90)
    if date.today().month > 6:
      minorticklocations.append(tickday + 181)
      if date.today().month > 9:
        minorticklocations.append(tickday + 273)
  ax1.set_yticks(majorticklocations)
  ax1.set_yticklabels(ticklabels, rotation=0)
  ax1.yaxis.set_minor_locator(ticker.FixedLocator(minorticklocations))

  ticks = np.linspace(1/7, 6/7, 6)
  labels = ['1', '5', '10', '25', '50', '75']
  fig.colorbar(cm.ScalarMappable(cmap=colormap('2d_hist_ratios_unscaled')), cax=ax2, ticks=ticks)
  ax2.set_yticklabels(labels)
  ax2.set_ylabel('Count', fontsize=15, rotation=270)
  ax2.yaxis.set_label_coords(3, 0.5)
  plt.show()
  fig.savefig('say_their_names/figures/decay/2dhist_ratios.pdf', bbox_inches='tight')




#cumulative_analysis('wapo')
#vertical_analysis()
#attention_decay('combined_curated', days=0, mode='absolute', metric='freq_norm', top=None, log=True)
"""
fig = plt.figure(figsize=[12, 6])
plt.subplot(221)
attention_decay('combined_curated', days=50, mode='peak', metric='r_rel', gender='Male', top=None, log=False)
plt.ylabel('Male', fontsize=15)
plt.subplot(222)
attention_decay('combined_curated', days=50, mode='incident', metric='r_rel', gender='Male', top=None, log=False)

plt.subplot(223)
attention_decay('combined_curated', days=50, mode='peak', metric='r_rel', gender='Female', top=None, log=False)
plt.xlabel('Days after peak', fontsize=15)
plt.ylabel('Female', fontsize=15)
plt.subplot(224)
attention_decay('combined_curated', days=50, mode='incident', metric='r_rel', gender='Female', top=None, log=False)
plt.xlabel('Days after incident', fontsize=15)
plt.annotate('Normalized frequency', (0.04, 0.5), xycoords='figure fraction', fontsize=15, rotation=90, ha='center', va='center')

plt.show()
fig.savefig('say_their_names/figures/decay/ratios_peak_and_incident.pdf', bbox_inches='tight')
"""
#min_rank_graph('combined_curated', 'All')
#name_rank_plots('combined_curated', False, 'All')
#name_rank_updates('combined_curated', 7)
#spike_analysis('combined_curated', 'All')
#compare_databases()
#combine_databases()
#heatmap(database='combined_curated', overwrite=True, top=50, metric='min_rank', annotate=25, dropna=True)
ratios('combined_curated')