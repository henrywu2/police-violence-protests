from datetime import datetime
import numpy as np
import pandas as pd
from sklearn.neighbors import NearestNeighbors
import scipy.stats
import string
pd.set_option('max_rows', 50000)

"""
def parse_name(ngram):
  if ngram == ngram and ngram != 'Name withheld by police':
    formatted_name = ngram.replace('.', '').replace('"', '').replace(',', '').replace('(', '').replace(')', '').split()
    formatted_name = ' '.join([formatted_name[0], formatted_name[-2]]) if (formatted_name[-1] == 'Jr' or formatted_name[-1] == 'Sr' or formatted_name[-1] == 'II' or formatted_name [-1] == 'III' or formatted_name [-1] == 'IV') else ' '.join([formatted_name[0], formatted_name[-1]])
  else:
    formatted_name = '_UNKNOWN_NAME_'
  return formatted_name


df = pd.read_csv('say_their_names/FATAL_ENCOUNTERS_RAW.csv')
df = df.rename(columns={
    'Name':'name',
    'Unique ID':'id',
    'Date of injury resulting in death (month/day/year)':'date',
    'Gender':'gender',
    'Highest level of force':'highest force',
    'Armed or Unarmed':'armed',
    'Alleged weapon':'weapon',
    'Physical movement':'movement',
    'Fleeing/Not fleeing':'fleeing',
    'Intended use of force (Developing)':'intended force'
})
df = df.loc[df['Race with imputations'] == 'African-American/Black']
df['date'] = pd.to_datetime(df['date'], infer_datetime_format=True)
df = df.loc[df['date'] >= datetime(2009, 1, 1)]

nr = pd.read_csv('say_their_names/fatal_encounters_name_rank.csv').set_index('Unnamed: 0')

final = pd.DataFrame(columns=df.columns)
for i in df.index:
    #Format names
    formatted_name = parse_name(df.at[i, 'name'])
    df.at[i, 'name'] = formatted_name

    if formatted_name in nr.index and formatted_name not in ['Thomas Lane', 'Darren Wilson', '_UNKNOWN_NAME_']:
        final.loc[i, :] = df.loc[i, :]
        final.at[i, 'min_rank'] = nr.at[formatted_name, 'min_rank']

final.to_csv('say_their_names/classification_df.csv')
"""
final = pd.read_csv('say_their_names/classification_df.csv')
final = final.iloc[:, [2, 3, 4, 9, 19, 22, 23, 24, 25, 30, 47]]
dummy = lambda x: 1 if x == x else 0
categorical = ['gender', 'highest force', 'armed', 'weapon', 'movement', 'fleeing', 'intended force']

for i in final.index:
    final.at[i, 'date'] = (pd.to_datetime(final.at[i, 'date'], infer_datetime_format=True) - datetime(2009, 1, 1)).days
    final.at[i, 'min_rank'] = dummy(final.at[i, 'min_rank'])
    final.at[i, 'num_letters'] = len(final.at[i, 'name'])

    for j in categorical:
        if final.at[i, j] == final.at[i, j]:
            final.at[i, j] = final.at[i, j].lower().translate(str.maketrans('', '', string.punctuation))
    
    """
    if final.at[i, 'gender'] == 'transgender':
        final.at[i, 'gender'] = float('nan')
    """

    if final.at[i, 'armed'] == 'uncertain':
        final.at[i, 'armed'] = float('nan')
    
    if final.at[i, 'weapon'] == final.at[i, 'weapon']:
        if final.at[i, 'weapon'].startswith('blunt'):
            final.at[i, 'weapon'] = 'blunt object'
        elif final.at[i, 'weapon'].startswith('edged') or final.at[i, 'weapon'] in ['box cutter', 'sword']:
            final.at[i, 'weapon'] = 'edged weapon'
        elif final.at[i, 'weapon'].startswith('other') or final.at[i, 'weapon'] in ['vehicle', 'taser']:
            final.at[i, 'weapon'] = 'other'
        elif 'gun' in final.at[i, 'weapon'] or final.at[i, 'weapon'] == 'rifle':
            final.at[i, 'weapon'] = 'gun'
    
    if final.at[i, 'movement'] == final.at[i, 'movement']:
        if final.at[i, 'movement'].startswith('advanced'):
            final.at[i, 'movement'] = 'advanced upon officers'
        elif final.at[i, 'movement'].startswith('uncertain'):
            final.at[i, 'movement'] = float('nan')
        elif final.at[i, 'movement'].startswith('vehic'):
            final.at[i, 'movement'] = 'vehicle'
        elif final.at[i, 'movement'].startswith('sudden'):
            final.at[i, 'movement'] = 'sudden threatening movement'
        elif final.at[i, 'movement'].startswith('struggle'):
            final.at[i, 'movement'] = 'struggle'
        elif final.at[i, 'movement'].startswith('self'):
            final.at[i, 'movement'] = 'selfinflicted injury'
        elif final.at[i, 'movement'].startswith('none'):
            final.at[i, 'movement'] = 'none'

    if final.at[i, 'fleeing'] in ['armed', 'unarmed', 'vehicle foot', 'uncertain', 'fleeing', 'gunshot']:
        final.at[i, 'fleeing'] = float('nan')
    elif final.at[i, 'fleeing'] in ['bicycle', 'minibike', 'motorcycle']:
        final.at[i, 'fleeing'] = 'vehicle'

    if final.at[i, 'intended force'] in ['vehicle', 'pursuit']:
        final.at[i, 'intended force'] = 'vehicpurs'
    elif final.at[i, 'intended force'] == 'undetermined':
        final.at[i, 'intended force'] = float('nan')

df_more_attention = final.loc[final['min_rank'] > 0]
df_less_attention = final.loc[final['min_rank'] < 1]

for i in categorical:
    tab = pd.crosstab(final[i], final['min_rank'])
    chi2 = scipy.stats.chi2_contingency(tab)
    print('{}\n\n{}\n\n'.format(tab, chi2))

for j in ['Age', 'date', 'num_letters']:
    t = scipy.stats.ttest_ind(df_less_attention[j], df_more_attention[j], nan_policy='omit')
    print('{}: Less attention: {}\nMore attention: {}\n{}\n\n'.format(j, df_less_attention[j].mean(), df_more_attention[j].mean(), t))