from datetime import datetime, timedelta
from storywrangling import Storywrangler
import matplotlib.pyplot as plt
import requests
import json
import numpy as np
import pandas as pd
import matplotlib.ticker as ticker
pd.set_option('display.max_rows', 10000)

storywrangler = Storywrangler()
lang = storywrangler.get_lang("en")

def plot_one_ratio(ngram):
    d = storywrangler.get_ngram(
        ngram, "en")
    n = len(ngram.split())

    d["lang_count"] = lang["count"]
    d["lang_count_no_rt"] = lang["count_no_rt"]

    d[f"lang_num_ngrams"] = lang[f"num_{n}grams"]
    d[f"lang_num_ngrams_no_rt"] = lang[f"num_{n}grams_no_rt"]

    d[f"lang_unique_ngrams"] = lang[f"unique_{n}grams"]
    d[f"lang_unique_ngrams_no_rt"] = lang[f"unique_{n}grams_no_rt"]


    ratio = (d['count'] - d['count_no_rt']) / d['count']
    lang_ratio = (d['lang_num_ngrams'] - d['lang_num_ngrams_no_rt']
                ) / d['lang_num_ngrams']

    d["r_rel"] = ratio / lang_ratio
    d["r_rel"] = d["r_rel"].replace([np.inf, -np.inf, np.nan], 1)

    #print(d)

    plt.plot(d["r_rel"].rolling(30, center=True).mean(), label=ngram)#.rolling(7, center=True).mean())

for i in ["Trayvon Martin"]:
    plot_one_ratio(i)

plt.legend()
plt.show()

"""
at = d['count'].resample(t1).mean()
ot = d['count_no_rt'].resample(t1).mean()
rt = at - ot
"""
