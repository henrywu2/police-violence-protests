import contagiograms
from datetime import datetime

terms = ["Black Lives Matter", "All Lives Matter", "Blue Lives Matter", "#BlackLivesMatter", "#AllLivesMatter", "#BlueLivesMatter", "White Lives Matter", "Police Lives Matter", "Lives Matter", "#WhiteLivesMatter", "#PoliceLivesMatter", "lives matter"]

for i in range(len(terms)):
    terms[i] = [terms[i], 'en']

ngrams = {"various_lives_matter": terms}

contagiograms.plot(ngrams, 'lives_matter/figures', start_date=datetime(2020, 5, 1), t1="1W", t2=7)

blm_all_time = {
    "blm_long_term": [
        ["Black Lives Matter", "en"], ["#BlackLivesMatter", "en"]
    ]
}

contagiograms.plot(blm_all_time, 'lives_matter/figures', start_date=datetime(2014, 8, 10), t1="1M", t2=30)