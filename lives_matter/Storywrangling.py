from datetime import datetime
from storywrangling import Storywrangler
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
pd.set_option('display.max_rows', 10000)

storywrangler = Storywrangler()

terms = ["Black Lives Matter", "All Lives Matter", "Blue Lives Matter", "#BlackLivesMatter", "#AllLivesMatter", "#BlueLivesMatter", "White Lives Matter", "Police Lives Matter", "Lives Matter", "#WhiteLivesMatter", "#PoliceLivesMatter", "lives matter"]

for i in terms:
  name_ngram['lang_count'] = lang['count']
  name_ngram['lang_count_no_rt'] = lang['count_no_rt']

  name_ngram['lang_num_2grams'] = lang['num_2grams']
  name_ngram['lang_num_2grams_no_rt'] = lang['num_2grams_no_rt']

  ratio = (name_ngram['count'] - name_ngram['count_no_rt']) / name_ngram['count']
  lang_ratio = (name_ngram['lang_num_2grams'] - name_ngram['lang_num_2grams_no_rt']
              ) / name_ngram['lang_num_2grams']

  name_ngram['r_rel'] = ratio / lang_ratio
  name_ngram['r_rel'] = name_ngram['r_rel'].replace([np.inf, -np.inf, np.nan], 1)

  ratios.loc[formatted_name, :] = name_ngram['r_rel']