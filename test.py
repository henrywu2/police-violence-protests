import numpy as np
from datetime import datetime, timedelta
from storywrangling import Storywrangler
import contagiograms
import matplotlib.pyplot as plt
from matplotlib import cm
import matplotlib as mpl
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
import pandas as pd
import seaborn as sns
import tweet_query
import re

pd.set_option('display.max_rows', 50000)
"""
names = ['#BlackOutTuesday', '#BlackoutTuesday', '#blackouttuesday', 'blackout']
for i in range(len(names)):
    names[i] = [names[i], 'en']

ngrams = { '1': names }

contagiograms.plot(
    ngrams,
    'say_their_names/../',
    start_date=datetime(2020, 5, 1),
    t1='1W',
    t2=7
)
"""
storywrangler = Storywrangler()
"""
names = pd.read_csv('say_their_names/combined_curated.csv')
for i in names['Unnamed: 0']:
  ngram = pd.read_csv(f'say_their_names/combined_curated/{i}.csv')
  plt.scatter(ngram['rank'], ngram['r_rel'], s=5, c='b', alpha=0.2)
plt.xlabel('Rank')
plt.gca().invert_xaxis()
#plt.xscale('log')
plt.ylabel('R_rel')
#plt.title('"Trayvon Martin"')
plt.show()
"""
"""
print(storywrangler.get_ngram('Davon Crawford', 'en'))

print(storywrangler.get_zipf_dist(
  date=datetime(2020, 9, 6),
  lang='en',
  database='2grams',
  max_rank=100
))
print(storywrangler.get_zipf_dist(
  date=datetime(2020, 5, 29),
  lang='en',
  database='2grams',
  max_rank=100
))

famous_people_list = ['George Floyd', 'Robin Williams', 'Kobe Bryant', 'Donald Trump', 'Justin Bieber', 'Joe Biden', 'Michael Jackson', 'Whitney Houston', 'Mac Miller', 'Harry Styles', 'David Bowie']
famous_people = pd.DataFrame(index=famous_people_list, columns=['min_rank', 'min_rank_date', 'max_count', 'max_count_date'])
for i in famous_people.index:
  ngram = storywrangler.get_ngram(
    i,
    "en"
  )
  famous_people.loc[i, :] = [ngram['rank'].min(), ngram['rank'].idxmin(), ngram['count'].max(), ngram['count'].idxmax()]
print(famous_people.sort_values(by='max_count'))

most_common = pd.DataFrame(columns=['count', 'count_no_rt', 'rank', 'rank_no_rt', 'freq', 'freq_no_rt', 'ngram'])
for i in pd.date_range(datetime(2011, 1, 1), datetime.today() - timedelta(days=3)):
  test = storywrangler.get_zipf_dist(
    date=i,
    lang='en',
    database='2grams',
    max_rank=10
  )
  for j in range(len(test.index)):
    if re.search('^[A-Z][a-z]+ [A-Z][a-z]+$', test.index[j]):# and test.index[j] not in ['Top Social', 'Social Artist', 'Merry Christmas', 'Las Vegas', 'Get Weather', 'Weather Updates', 'Super Bowl', 'El Paso', 'New Zealand']:
      print(test.index[j])
      most_common.loc[str(i) + str(j), :] = test.iloc[j, :]
      most_common.at[str(i) + str(j), 'ngram'] = test.index[j]
most_common.sort_values(by='count', ascending=False).to_csv('top_ten_2_grams.csv')


df = pd.read_csv('say_their_names/combined_name_rank.csv')
df['name_start_date'] = pd.to_datetime(df['name_start_date'], infer_datetime_format=True)
plt.scatter(df['name_start_date'], df['min_rank'])
plt.yscale('log')
plt.show()


test_spike = storywrangler.get_zipf_dist(
  date=datetime(2020, 6, 1),
  lang="en",
  database="3grams"
)
test_spike["count_rt"] = test_spike["count"] - test_spike["count_no_rt"]
print(test_spike.sort_values(by="count_rt", ascending=False).iloc[0:1000][:])


start_date = datetime(2020, 1, 1)
end_date = datetime(2021, 2, 22)
freq='D'
dates = pd.date_range(start_date, end_date, freq=freq)
anchor='Capitol'
counters = tweet_query.counters.Counters(dates, anchor)
counters.get(save=True)

counters.aggregate('W')

counters.plot_sentiment_timeseries()

date1 = datetime(2020, 6, 1)
"""