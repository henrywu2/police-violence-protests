from datetime import datetime, date
from storywrangling import Storywrangler
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.stats
pd.set_option('display.max_rows', 10681)

storywrangler = Storywrangler()

#getting "Black Lives Matter" and "#BlackLivesMatter" n-grams
blm = storywrangler.get_ngram(
  'Black Lives Matter',
  start_time=datetime(2017, 1, 15),
  end_time=datetime(2021, 1, 31),
  lang='en',
).fillna(0)
hashtag_blm = storywrangler.get_ngram(
  '#BlackLivesMatter',
  start_time=datetime(2017, 1, 15),
  end_time=datetime(2021, 1, 31),
  lang='en',
).fillna(0)

#getting and formatting racial justice protest data
protests_raw = pd.read_csv('https://countlove.org/data/data.csv')
protests_raw['Date'] = pd.to_datetime(protests_raw['Date'], infer_datetime_format=True)
cutoff = protests_raw.iat[-1, 0]
protests_raw['bool'] = False
for i in protests_raw.index:
  protests_raw.iloc[i, 8] = 'racial' in protests_raw.iloc[i, 4]
protests_raw = protests_raw.loc[protests_raw['bool']]
protests_count = protests_raw['Date'].value_counts().to_frame().sort_index()
protests_count_final = pd.DataFrame(index=pd.date_range(start='1/15/2017', end=cutoff), columns=['count'])
protests_count_final['count'] = 0
for i in protests_count.index:
  protests_count_final.at[i, 'count'] = protests_count.at[i, 'Date']
#protests_count_final = protests_count_final.loc[:date(2020, 5, 25), :]

draft = pd.read_csv('daily_protests/ACLED.csv')
draft = draft.loc[draft['assoc_actor_1'] == draft['assoc_actor_1']]
for k in draft.index:
  draft.at[k, 'bool'] = 'BLM' in draft.at[k, 'assoc_actor_1']
draft_blm = draft.loc[draft['bool']]
#print(draft_blm[['event_date', 'assoc_actor_1']])
draft_blm['event_date'] = pd.to_datetime(draft_blm['event_date'], infer_datetime_format=True)
draft_count = draft_blm['event_date'].value_counts().to_frame().sort_index()
draft_final = pd.DataFrame(index=pd.date_range(start='1/1/2020', end='3/12/2021'), columns=['count'])
draft_final['count'] = 0
for l in draft_count.index:
  draft_final.at[l, 'count'] = draft_count.at[l, 'event_date']

plt.plot(draft_final['count'])
plt.plot(protests_count_final['count'])
plt.show()

#plotting all data
e = plt.figure(figsize=[12, 4])
plt.subplot(311)
plt.plot(blm['freq'], label='"Black Lives Matter')
plt.ylabel('Frequency', fontsize=15)
plt.yscale('log')
plt.subplot(312)
plt.plot(hashtag_blm['freq'], label='"#BlackLivesMatter"')
plt.ylabel('Frequency', fontsize=15)
plt.yscale('log')
plt.subplot(313)
plt.plot(protests_count_final['count'])
plt.xlabel('Date', fontsize=15)
plt.ylabel('Racial justice\nprotests', fontsize=15)
plt.yscale('log')
plt.show()
e.savefig('daily_protests/protests_terms_lineplots.pdf', bbox_inches='tight')
e.savefig('daily_protests/protests_terms_lineplots.png', bbox_inches='tight')


def lag_graphs(blm, hashtag_blm, protests_count_final):
  blm_lag = pd.DataFrame(columns=['pearson r', 'pearson p', 'spearman r', 'spearman p'])
  hashtag_blm_lag = pd.DataFrame(columns=['pearson r', 'pearson p', 'spearman r', 'spearman p'])
  both_lag = pd.DataFrame(columns=['pearson r', 'pearson p', 'spearman r', 'spearman p'])
  for i in range(-10, 11):
    if i < 0:
      blm_trim = blm.iloc[i * -1:, :]
      hashtag_blm_trim = hashtag_blm.iloc[i * -1:, :]
      protests_count_final_trim = protests_count_final.iloc[:i, :]
    elif i > 0:
      blm_trim = blm.iloc[:i * -1, :]
      hashtag_blm_trim = hashtag_blm.iloc[:i * -1, :]
      protests_count_final_trim = protests_count_final.iloc[i:, :]
    else:
      blm_trim = blm
      hashtag_blm_trim = hashtag_blm
      protests_count_final_trim = protests_count_final

    pr, pp = scipy.stats.pearsonr(blm_trim['freq'], protests_count_final_trim['count'])
    sr, sp = scipy.stats.spearmanr(blm_trim['freq'], protests_count_final_trim['count'])
    blm_lag.loc[i, :] = [pr, pp, sr, sp]
    if i == 0:
      plt.subplot(1, 3, 1)
      plt.scatter(blm_trim['freq'], protests_count_final_trim['count'])
      plt.xlabel('"Black Lives Matter" freq')
      plt.ylabel('Racial justice protests')
      plt.title('Lag = 0, r = ')

    pr, pp = scipy.stats.pearsonr(hashtag_blm_trim['freq'], protests_count_final_trim['count'])
    sr, sp = scipy.stats.spearmanr(hashtag_blm_trim['freq'], protests_count_final_trim['count'])
    hashtag_blm_lag.loc[i, :] = [pr, pp, sr, sp]
    if i == 0:
      plt.subplot(1, 3, 2)
      plt.scatter(hashtag_blm_trim['freq'], protests_count_final_trim['count'])
      plt.xlabel('"#BlackLivesMatter" freq')
      plt.ylabel('Racial justice protests')
      plt.title('Lag = 0, r = ')

  for j in range(-10, 11):
    if j < 0:
      blm_trim = blm.iloc[:j, :]
      hashtag_blm_trim = hashtag_blm.iloc[j * -1:, :]
    elif j > 0:
      blm_trim = blm.iloc[j:, :]
      hashtag_blm_trim = hashtag_blm.iloc[:j * -1, :]
    else:
      blm_trim = blm
      hashtag_blm_trim = hashtag_blm
    
    pr, pp = scipy.stats.pearsonr(hashtag_blm_trim['freq'], blm_trim['freq'])
    sr, sp = scipy.stats.spearmanr(hashtag_blm_trim['freq'], blm_trim['freq'])
    both_lag.loc[j, :] = [pr, pp, sr, sp]
    if j == 0:
      plt.subplot(1, 3, 3)
      plt.scatter(hashtag_blm_trim['freq'], blm_trim['freq'])
      plt.xlabel('"#BlackLivesMatter" freq')
      plt.ylabel('"Black Lives Matter" freq')
      plt.title('Lag = 0, r = ')

  plt.show()

  return blm_lag, hashtag_blm_lag, both_lag

a1, a2, a3 = lag_graphs(blm, hashtag_blm, protests_count_final)

blm['freq'] = blm['freq'] + 10**-7
protests_count_final['count'] = protests_count_final['count'] + 1
blm['freq'] = np.log10(blm['freq'])
hashtag_blm['freq'] = np.log10(hashtag_blm['freq'])
protests_count_final['count'] = np.log10(protests_count_final['count'])

b1, b2, b3 = lag_graphs(blm, hashtag_blm, protests_count_final)
f = plt.figure()
def lag_plot(df):
  plt.plot(df['pearson r'], label='Pearson')
  plt.plot(df['spearman r'], label='Spearman')
  MAX = round(df['pearson r'].max(), 3)
  IMAX = df['pearson r'].values.argmax() - 10
  plt.annotate(f'({IMAX}, {MAX})', (IMAX, MAX), xytext=(0, 5), textcoords='offset pixels', ha='center')
  MAX = round(df['spearman r'].max(), 3)
  IMAX = df['spearman r'].values.argmax() - 10
  plt.annotate(f'({IMAX}, {MAX})', (IMAX, MAX), xytext=(0, 5), textcoords='offset pixels', ha='center')
  plt.ylim(bottom=0.2, top=0.95)

plt.subplot(231)
lag_plot(a1)
plt.ylabel('No transform')
plt.legend()
plt.title('"Black Lives Matter"\nand protests', fontsize=15)

plt.subplot(232)
lag_plot(a2)
plt.title('"#BlackLivesMatter"\nand protests', fontsize=15)

plt.subplot(233)
lag_plot(a3)
plt.title('"#BlackLivesMatter"\nand "Black Lives Matter"', fontsize=15)

plt.subplot(234)
lag_plot(b1)
plt.ylabel('Log transform')

plt.subplot(235)
lag_plot(b2)
plt.xlabel('Lag (days)', fontsize=15)

plt.subplot(236)
lag_plot(b3)

plt.annotate('Correlation coefficient', (0.04, 0.5), xycoords='figure fraction', fontsize=15, rotation=90, ha='center', va='center')
plt.show()
f.savefig('daily_protests/lag_graphs.pdf', bbox_inches='tight')
f.savefig('daily_protests/lag_graphs.png', bbox_inches='tight')