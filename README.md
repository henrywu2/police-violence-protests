# Police violence protests
The purpose of this project is to examine the recent wave of protests against police brutality associated with the Black Lives Matter movement in the United States.
Paper available at https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0279225.
