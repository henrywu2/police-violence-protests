from datetime import datetime, timedelta, date
from storywrangling import Storywrangler
import matplotlib.pyplot as plt
import requests
import json
import numpy as np
import pandas as pd
import matplotlib.ticker as ticker
pd.set_option('display.max_rows', 10000)

storywrangler = Storywrangler()

DATES = [[datetime(2014, 11, 24), datetime(2014, 12, 8)],
    [datetime(2015, 7, 13), datetime(2015, 7, 26)],
    [datetime(2016, 7, 5), datetime(2016, 7, 13)],
    [datetime(2017, 8, 12), datetime(2017, 8, 22)], 
    [datetime(2020, 5, 25), datetime(2020, 6, 7)]]

START_YEAR = 2020

def is_leap_year(year):
  if year % 4 != 0:
    return False
  elif year % 100 == 0:
    if year % 400 == 0:
      return True
    else:
      return False
  else:
    return True

uri = 'http://hedonometer.org/api/v1/happiness/?format=json&timeseries__title=en_all&date__gte=2009-01-01&limit=10000'
r = requests.get(uri)
df = pd.DataFrame(json.loads(r.content)['objects']).set_index('date')
df.index = pd.to_datetime(df.index, infer_datetime_format=True)
df = df.sort_index()
df['happiness'] = df['happiness'].astype(float)
print(df)
print(df.loc[:datetime(2020, 12, 31), 'happiness'].mean())

lang = storywrangler.get_lang("en")
lang.loc[datetime(2016, 12, 8), :] = float('nan')
lang.loc[datetime(2016, 12, 28):datetime(2016, 12, 31), :] = float('nan')
print(lang.loc[datetime(2020, 5, 1):datetime(2020, 5, 26), 'count_no_rt'].mean())
print(lang.loc[datetime(2020, 5, 27):datetime(2020, 6, 7), 'count_no_rt'].mean())
print(lang.loc[datetime(2020, 5, 1):datetime(2020, 5, 26), 'retweets'].mean())
print(lang.loc[datetime(2020, 5, 27):datetime(2020, 6, 7), 'retweets'].mean())
print(lang.loc[datetime(2020, 5, 1):datetime(2020, 5, 26), 'count'].mean())
print(lang.loc[datetime(2020, 5, 27):datetime(2020, 6, 7), 'count'].mean())

hashtag_blm = storywrangler.get_ngram(
  '#BlackLivesMatter',
  'en'
)
blm = storywrangler.get_ngram(
  'Black Lives Matter',
  'en'
)
for i in [df, lang, blm]:
  i.index = (i.index - datetime(START_YEAR, 1, 1)).days

alpha = 0.5

f = plt.figure(figsize=[10, 4])
ax = plt.axes([0.05, 0.43, 0.9, 0.35])
ax.plot(lang['count_no_rt'], 'tab:blue', alpha=alpha)
ax.plot(lang['count_no_rt'].rolling(7, center=True).mean(), 'tab:blue', label = "OT")
ax.plot(lang['retweets'], 'tab:orange', alpha=alpha)
ax.plot(lang['retweets'].rolling(7, center=True).mean(), 'tab:orange', label = "RT")
ax.legend()
ax.yaxis.set_major_locator(ticker.FixedLocator(range(2000000, 16000000, 2000000)))
plt.xlim(left=0, right=(datetime(2021, 5, 31) - datetime(START_YEAR, 1, 1)).days)
plt.ylim(bottom=0)
plt.ylabel('Tweets (1e7)', fontsize=15)

ax2 = plt.axes([0.05, 0.08, 0.9, 0.35])
ax2.plot(df['happiness'], 'tab:green', alpha=alpha)
ax2.plot(df['happiness'].rolling(7, center=True).mean(), 'tab:green')
plt.xlabel('Date', fontsize=15)
plt.ylabel('Expressed happiness', fontsize=15)
plt.xlim(left=0, right=(datetime(2021, 5, 31) - datetime(START_YEAR, 1, 1)).days)
plt.ylim(bottom=5.51)

"""
majorticklocations = [0]
minorticklocations = []
ticklabels = []
tickday = 0
for year in range(START_YEAR, 2021):
  ticklabels.append(year)
  if is_leap_year(year):
    tickday += 1
  minorticklocations.extend([tickday + 90, tickday + 181, tickday + 273])
  tickday += 365
  majorticklocations.append(tickday)
ticklabels.append(2021)
if date.today().month > 3:
  minorticklocations.append(tickday + 90)
  if date.today().month > 6:
    minorticklocations.append(tickday + 181)
    if date.today().month > 9:
      minorticklocations.append(tickday + 273)
ax2.set_xticks(majorticklocations)
ax2.set_xticklabels(ticklabels, rotation=0)
ax2.xaxis.set_minor_locator(ticker.FixedLocator(minorticklocations))
"""

majorticklocations = [0, 91, 182, 274, 366, 456]
minorticklocations = [31, 60, 121, 152, 213, 244, 305, 335, 397, 425, 486]
ticklabels = [2020, 'April', 'July', 'October', 2021, 'April']
ax2.set_xticks(majorticklocations)
ax2.set_xticklabels(ticklabels, rotation=0)
ax2.xaxis.set_minor_locator(ticker.FixedLocator(minorticklocations))

ax3 = plt.axes([0.05, 0.78, 0.9, 0.28])
ax3.plot(blm['freq'], 'black', alpha=alpha)
ax3.plot(blm['freq'].rolling(7, center=True).mean(), 'black', label='"Black Lives Matter"')
ax3.legend()
plt.ylabel('Rel. freq.\namong 3-grams', fontsize=15)
#plt.gca().yaxis.set_label_coords(-0.04, 0.5)
plt.yscale('log')
plt.xlim(left=0, right=(datetime(2021, 5, 31) - datetime(START_YEAR, 1, 1)).days)
plt.ylim(bottom=10**-7.5, top=10**-2.5)
#plt.title(f'Retweets and Original Tweets (10% Random Sample, English, {str(START_YEAR)}-2021) with Happiness')

area_start = None
area_end = None
cutoff = 5000
for i in blm.index[:-1]:
  if blm.at[i, 'rank'] > cutoff and blm.at[i + 1, 'rank'] <= cutoff:
    area_start = i + 1
  elif blm.at[i, 'rank'] <= cutoff and blm.at[i + 1, 'rank'] > cutoff:
    area_end = i
  if area_end:
    ax3.axvspan(area_start, area_end, edgecolor=None, facecolor='0.9')
    ax2.axvspan(area_start, area_end, edgecolor=None, facecolor='0.9')
    ax.axvspan(area_start, area_end, edgecolor=None, facecolor='0.9')
    area_end = None

"""
for window in DATES:
  ax3.axvspan((window[0] - datetime(START_YEAR, 1, 1)).days, (window[1] - datetime(START_YEAR, 1, 1)).days, edgecolor=None, facecolor='0.9')
  ax2.axvspan((window[0] - datetime(START_YEAR, 1, 1)).days, (window[1] - datetime(START_YEAR, 1, 1)).days, edgecolor=None, facecolor='0.9')
  ax.axvspan((window[0] - datetime(START_YEAR, 1, 1)).days, (window[1] - datetime(START_YEAR, 1, 1)).days, edgecolor=None, facecolor='0.9')
"""

ax2.xaxis.grid(color='0', alpha=0.4)
for axis in [ax, ax3]:
  axis.set_xticks(majorticklocations)
  axis.set_xticklabels([])
  axis.xaxis.grid(color='0', alpha=0.4)
  axis.tick_params(axis='x', colors=(0, 0, 0, 0))
plt.show()

f.savefig('spike/spike.pdf', bbox_inches='tight')
f.savefig('spike/spike.png', bbox_inches='tight')

"""
lang["ratio"] = lang["retweets"] / lang["count_no_rt"]
plt.plot(lang["ratio"])
plt.xlabel("Date")
plt.title("Ratio of Retweets to Original Tweets, English, 2009-2020")
plt.show()

blackout_tuesday_3grams = storywrangler.get_zipf_dist(
  date=datetime(2020, 6, 2),
  lang="en",
  database="3grams"
)
blackout_tuesday_3grams = blackout_tuesday_3grams.sort_values(by="count_no_rt", ascending=False)

df = np.zeros((blackout_tuesday_3grams.shape[0], 3))
df = pd.DataFrame(data=df, index=blackout_tuesday_3grams.index, columns=["Spike count", "Base count", "Ratio"])

for i in range(1000):#blackout_tuesday_1grams.shape[0]):
  term = blackout_tuesday_3grams.index[i]
  ngram_spike = storywrangler.get_ngram(
    term,
    lang="en",
    start_time=datetime(2020, 5, 27),
    end_time=datetime(2020, 6, 7)
  )
  spike_count_mean = ngram_spike.loc[:, ["count_no_rt"]].mean()
  df.iloc[i][0] = spike_count_mean

  ngram_base = storywrangler.get_ngram(
    term,
    lang="en",
    start_time=datetime(2020, 3, 31),
    end_time=datetime(2020, 5, 23)
  )
  base_count_mean = ngram_base.loc[:, ["count_no_rt"]].mean()
  df.iloc[i][1] = base_count_mean

  df.iloc[i][2] = spike_count_mean/base_count_mean
print(df.iloc[0:1000][:])
print(df.sort_values(by="Ratio", ascending=False).iloc[0:1000][:])
"""